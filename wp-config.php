<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'F9i4Q7JaFosdPL8PIqb9WtJhExjuKhq9+Jg5pPhgQ/GlM99cw37wEXUG5t9Hs7GvGCzodAsGiiL143nvuD/ipw==');
define('SECURE_AUTH_KEY',  'hM5Oau9dqVulI7TDKH1xTc3iMNEREJdL0khLsJ+p6l1YZfRSd9QBuOhOey6KMhvmwJi8NTs0d3lUb4Abrt61jw==');
define('LOGGED_IN_KEY',    '+tQfBUcAyMdqmUG8je6On5oTuGQTor5ZcQXBgGumSQJJk7V0q1xrIZe39qN7ofJMSwncVyYwoDO9DLdQm9D7Tw==');
define('NONCE_KEY',        'k2R1yA3k4AzDqOtEC9BCqDfzogYFFMhihtx6o/Pn71IZBKYhdwvM2UYhbfDM113whtnToEljqfsWKN9Ez+oVjg==');
define('AUTH_SALT',        '3WO9rnBicLyu1WRg9PUiLx5Q3Pixqejw0dprdhOK8F2GVqMta293fdoJkkX/P9q500iKVM9WkJU5/TzFmEQcPA==');
define('SECURE_AUTH_SALT', 'x7yjHI1XTq/bN/zisyUsdvkppuXNvlxK/k+ixmNQ0XHTUNXJArnzCiS4PHulGce/S++8xYNnOxwpxXK2h9lD+w==');
define('LOGGED_IN_SALT',   'Qrz5/v+60mw7n+VPZOhEb6ueYD/ZuhFFPhueNHTLmKUUsnYjVhg2fhXTQX6fYS9260k3mi1+nU7A1UcyNmishg==');
define('NONCE_SALT',       'fRHajWRSzLgcF82w0zj2Sed/3N5/mDrRYLPEp/+GFljOGnHaGZCnJinHf2vS4+f7raqHQ1pVfTqcZZ2ORKTYDw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
