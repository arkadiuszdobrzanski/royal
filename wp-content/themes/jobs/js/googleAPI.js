function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    request = $.ajax({
        //uderzam do customowego routingu
        url: `https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.coords.latitude},${position.coords.longitude}&result_type=administrative_area_level_1&key=AIzaSyB832_bHfRQLmwHuIxiTy8K-5_REW1AAlY`,
        // url: "/wp-json/jobs/v1/jobsMainSearch",
        type: "GET",
        fail: function () {},
        success: function (data) {
            console.log(data.results[0].formatted_address);
            let searchValue = data.results[0].formatted_address;
            let method = "POST";

            let fill = "#left-near";
            let url = "/wp-json/jobs/v1/jobsSearchNear/";
            $.ajax({
                url: url,
                type: method,
                data: searchValue,
                fail: function () {},
                success: function (data) {
                    console.log(searchValue);
                    $(fill).hide().fadeOut(250).html(data).fadeIn(250);
                    $('#voivode').html(searchValue);
                },
            });
        },
    });
    // alert(position.coords.latitude + " " + position.coords.longitude);
}


var autocomplete;
function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('search-location')),
        {types: ['geocode']});

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();

}

var autocomplete;
function initAutocomplete2() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('selectLocation')),
        {types: ['geocode']});

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', fillInAddress2);
}

function fillInAddress2() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();

}