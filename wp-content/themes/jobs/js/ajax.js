$("#main-search-list").hide();
var delayValue = 400;
function delay(callback, ms) {
  var timer = 0;
  return function () {
    var context = this,
      args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}

function submitData(method, searchValue, url, fill) {
  request = $.ajax({
    //uderzam do customowego routingu
    url: url + "/" + searchValue,
    type: method,
    fail: function () {},
    success: function (data) {
      //w data jest zwracany cały wygląd HTML wraz z wprowadzonymi danymi
      console.log(searchValue);
      $(fill).hide().fadeOut(250).html(data).fadeIn(350);
      //Więc wrzucam 'data' do tej klasy
    },
  });
}

$("#search-job").keyup(
  delay(function (event) {
    let searchValue = $("#search-job").val();
    let method = $("#search-job").data("method");
    let fill = $("#search-job").data("fill");
    let url = $("#search-job").data("url");
    submitData(method, searchValue, url, fill);
  }, delayValue)
);

$("#main-search-input").keyup(
  delay(function (event) {
      if(event.which == 13) {
          return;
      }
    let searchValue = $("#main-search-input").val();
    let method = $("#main-search-input").data("method");
    let fill = $("#main-search-input").data("fill");
    let url = $("#main-search-input").data("url");
    submitData(method, searchValue, url, fill);
  }, delayValue)
);

$("#selectLocation").keyup(
    delay(function (event) {
        if(event.which == 13) {
            return;
        }
        let searchValue = $("#selectLocation").val();
        let method = $("#selectLocation").data("method");
        let fill = $("#selectLocation").data("fill");
        let url = $("#selectLocation").data("url");
        submitData(method, searchValue, url, fill);
    }, delayValue)
);

$("#selectCategory").on("change", function () {
  console.log(this.value);
  let method = $("#selectCategory").data("method");
  let fill = $("#selectCategory").data("fill");
  let url = $("#selectCategory").data("url");
  let searchValue = this.value;
  submitData(method, searchValue, url, fill);
});

$(".job-filter-wrapper")
  .find("a")
  .click(function () {
    console.log($(this).data("attr"));
    let method = "POST";
    let fill = "#job-filter-result";
    let url = "/wp-json/jobs/v1/jobsSearchBySide";
    let searchValue = $(this).data("attr");
    submitData(method, searchValue, url, fill);
  });

$("#clear").click(function () {
  console.log($(this).data("attr"));
  let method = "POST";
  let fill = "#job-filter-result";
  let url = "/wp-json/jobs/v1/jobsSearch/";
  let searchValue = "";
  submitData(method, searchValue, url, fill);
});

$(document).mouseup(function(e){
    var container = $("#main-search-list");

    // If the target of the click isn't the container
    if(!container.is(e.target) && container.has(e.target).length === 0){
        container.hide();
    }
});
