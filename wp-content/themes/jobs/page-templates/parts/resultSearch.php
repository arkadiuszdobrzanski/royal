<?php foreach($data as $d) { ?>
<div class="job-list">
    <div class="thumb">
        <a href="<?php echo get_the_permalink($d->ID); ?>">
            <img style="max-width:70px !important;" src="<?php echo get_the_post_thumbnail_url($d->ID);?>" class="img-fluid" alt="">
        </a>
    </div>
    <div class="body">
        <div class="content">
            <h4><a href="/job/<?php echo get_post($d->ID)->post_name; ?>"><?php echo get_the_title($d->ID); ?></a></h4>
            <div class="info">
                <span class="company"><a href="#"><i style="    color: rgba(220, 53, 69, 0.3);" class="<?php echo get_field('ikona',get_field('kategoria', $d)[0]->ID); ?>"> </i> <?php echo get_field('kategoria',$d)[0]->post_title; ?></a></span>
                <span class="office-location"><a href="#"><i data-feather="map-pin"></i><?php echo get_field('map',$d)['city']; ?>, <?php echo get_field('map',$d)['state']; ?></a></span>
                <span class="job-type temporary"><a href="#"><i data-feather="clock"></i><?php echo wpjm_get_the_job_types($d)[0]->name; ?></a></span>
            </div>
        </div>
        <div class="more">
            <div class="buttons">
                <a href="<?php echo get_the_permalink($d->ID); ?>" class="button" data-toggle="modal" data-target="#apply-popup-id">Przejrzyj ofertę</a>
                <a href="#" class="favourite"><i data-feather="heart"></i></a>
            </div>
            <p class="deadline">Wygasa: Oct 31,  2020</p>
        </div>
    </div>
</div>
<?php } ?>
<?php if(!$data){ ?>
    <p>Brak wyników</p>
<?php } ?>
