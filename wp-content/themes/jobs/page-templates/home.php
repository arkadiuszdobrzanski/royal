<?php
/*
 * Template Name: Strona główna
 */

get_header();

$result = ProductsService::getJobs();
$news = ProductsService::getNews();
$resultNear = ProductsService::getNearJobs();
$jobsCategories = ProductsService::getJobsCategories();
$locations = ProductsService::getLocations();


?>
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/main.css" />

    <!-- Banner -->
    <div id="loader-wrapper" style="text-align: center; display: flex; flex-direction: column; margin: 0 auto; justify-content: center; align-items: center;">
        <img style="z-index:9999; position: sticky; width:20%;" src="<?php bloginfo('template_directory'); ?>/images/logo_przyklad.png" alt="">
        <div id="loader"></div>

        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>

    </div>
    <div class="banner banner-1">
        <div class="img-fluid position-absolute" style="z-index: 100; width: 127px; height: 167px; top: -2px; right: -13px; background-image: url(<?php echo DefaultHelper::imageDir('kafel.png'); ?>);">

        </div>
        <div class="img-fluid position-absolute" style="z-index: -1; width: 212px; height: 166px; top: -26px; left: -63px; background-image: url(<?php echo DefaultHelper::imageDir('kafel2.png'); ?>);">

        </div>
        <div class="banner kropki" style="background-image: url(<?php echo DefaultHelper::imageDir('hero-anim/layer2.png'); ?>);">
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="banner-content pl-md-5 row" style="margin-top:1rem">
                        <div class="col-lg-6">
                            <h2 class="main-text text-left pl-sm-5"><span style="color:#FEB254; font-size:4.5rem;">Agencja pracy</span> <br> <span style="font-size:6.6rem; color:#FEB254">Royal Teamwork</span> <br>
                                <!--                        <img src="--><?php //echo DefaultHelper::imageDir('praca.svg') ?><!--" class="img-praca">-->
                                <!--                        <span class="text-praca">praca &nbsp;</span>-->
                                <span style="font-size:6.6rem; color:#515151">Łączymy</span> pracodawców<br>z pracownikami<span style="font-size:44px; color: #dc3545;">.</span>
                            </h2>
                                <div class="row hero-buttons hero-mobile py-2">
                                    <a href="/jobs-list" class="border col-6 bg-white button-left button-mobile" >
                                        <div class="row pt-5 pb-5 ">
                                            <div class="col-12">
                                                <h4>Znajdź prace</h4>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="/jobs-list" class="col-6 button-right button-mobile" >
                                        <div class="row pb-5 pt-5">
                                            <div class="col-12">
                                                <h4>Znajdź pracownika</h4>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <div class="row hero-button1 pt-2 mb-5 pb-5">
                                <a href="/rekrutacja" class="col-12 button-right" style="width:66%; flex:none">
                                    <div class="row pb-5 pt-5">
                                        <div class="col-12">
                                            <h4>Znajdź pracownika</h4>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <p class="subtext py-sm-3 ml-3 pl-3 my-sm-3 text-center" style="white-space: normal;">Z nami efektywna rekrutacja jest na wyciągnięcie ręki<br> - zarówno pracownika, jak i pracodawcy. </p>
                            <!--                        <a href="/jobs-list" class="button button-danger-bg">Zobacz oferty pracy w okolicy</a>-->
                        </div>
                        <div class="col-lg-6" style="background-image: url(<?php echo DefaultHelper::imageDir('worker.jpg'); ?>); background-size:cover">
                            <h2 class="main-text text-left pl-5" style="visibility: hidden"><span style="color:darkorange; font-size:4.5rem;">Agencja pracy</span> <br> <span style="font-size:6.6rem; color:darkorange">Royal Teamwork</span> <br>
                                <!--                        <img src="--><?php //echo DefaultHelper::imageDir('praca.svg') ?><!--" class="img-praca">-->
                                <!--                        <span class="text-praca">praca &nbsp;</span>-->
                                <span style="font-size:6.6rem; color:#515151">Łączymy</span> pracodawców<br>z pracownikami<span style="font-size:44px; color: #dc3545;">.</span>
                            </h2>
                            <div class="row hero-button2 pt-2 mb-5 pb-5">
                                <a href="/jobs-list" class="col-12 bg-white button-left" style="width:66%; flex:none">
                                    <div class="row pt-5 pb-5">
                                        <div class="col-12">
                                            <h4>Znajdź pracę</h4>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <p class="subtext py-sm-3 ml-3 pl-3 my-sm-3 text-center" style="visibility: hidden;">Z nami efektywna rekrutacja jest na wyciągnięcie ręki<br> - zarówno pracownika, jak i pracodawcy. </p>
                            <!--                        <a href="/jobs-list" class="button button-danger-bg">Zobacz oferty pracy w okolicy</a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner End -->


    <!-- Jobs -->
    <div class="list-jobs">
        <div class="container-fluid" style="background: linear-gradient(0deg, rgba(255,255,255,1) 22%, rgba(235, 244, 244,1) 79%);padding-bottom: 90px;">
            <div class="container about-us-content">
                <div>
                    <div class="row mb-2 mx-sm-0 my-sm-auto" style="padding-top:4rem;">
                        <div class="col mt-5">
                            <div class="section-header" style="margin-top: 12rem; margin-bottom: 0">
                                <h2>Praca szuka człowieka<span class="blur" style="left:25%;">Praca szuka człowieka?</span><span style="font-size:36px; color: #dc3545;">?</span><br>
                                    A może człowiek szuka pracy?
                                    U nas w końcu się odnajdą - i to szybko!
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2 mx-sm-0 my-sm-auto">
                        <div class="col-lg-6">
                            <img class="w-100" src="<?php echo DefaultHelper::imageDir('o_nas_royal-01.png') ?>" />
                        </div>
                        <div class="col-lg-6 d-flex aligncenter text-center justify-content-center align-items-center">
                            <h5 class="about-us-text mt-5" style="width: 95%">Skończ ze żmudnymi poszukiwaniami: przekopywaniem dziesiątek ofert pracy lub przeglądaniem sterty CV w poszukiwaniu kandydata, jakiego potrzebujesz.<br>
                                Z <span style="color: #dc3545;">Royal Teamwork</span> osiągniesz swój cel dużo prościej oraz szybciej - bez starty czasu i energii.
                                Jesteśmy agencją pracy, która łączy wykwalifikowanych pracowników ze sprawdzonymi firmami - sprawnie, skutecznie i w przyjaznej atmosferze.<br>
                                Po prostu powiedz, kogo szukasz, a resztą zajmiemy się my.</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Search and Filter -->
    <div class="searchAndFilter-wrapper mt-5" >
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="searchAndFilter-block">
                        <div class="searchAndFilter" style="z-index: 9;margin-top: 10rem;">
                            <form autocomplete="off" action="/jobs-list" class="search-form d-flex flex-lg-row flex-md-column flex-sm-column flex-column ">
                                <div class="main-search pr-5 w-100">
                                    <input data-method="POST" data-fill="#main-search-list" data-url="/wp-json/jobs/v1/jobsMainSearch" type="text" class="w-100" name="keyword" id="main-search-input" placeholder="Wprowadź stanowisko lub nazwę pracodawcy">
                                    <div id="main-search-list" class="position-absolute pt-5 pb-5 pl-5 pr-5 shadow-lg" style="width: 100%; z-index: 9;background: white;">
                                    </div>
                                </div>
                                <input type="text" class="w-100" name="location" id="search-location" placeholder="Lokalizacja" />

                                <button class="button danger-bg" type="submit"><i class="fas fa-search"> </i> Szukaj
                                </button>
                            </form>
                            <div class="filter-categories">
                                <h4>Działy</h4>
                                <ul>
                                    <?php foreach ($jobsCategories as $jobCategory) { ?>
                                        <li><a href="jobs-list/?cats=<?php echo get_the_title($jobCategory); ?>"><i class="<?php echo get_field('ikona', $jobCategory); ?>"></i><?php echo get_the_title($jobCategory); ?>
                                                <span>(<?php echo DefaultHelper::getCount($jobCategory); ?>)</span></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Search and Filter End -->


    <div>
        <div class="container-fluid" style="background: #f7f7f7;">
            <div class="container padding-top-90" style="padding-top:0px!important;">
                <div class="row ">
                    <div class="col">
                        <div class="nav nav-tabs job-tab d-flex justify-content-around">
                            </ul>
                            <div class="tab-content pt-5 pb-5 mt-3 mb-3 w-100" id="myTabContent">
                                <div class="tab-pane fade show active" id="recent" role="tabpanel" aria-labelledby="recent-tab">
                                    <div class="row my-0 mx-auto">
                                        <div class="col-lg-6  mt-3 mb-3 left" id="left-near">
                                            <h2 class="blur-par text-center">Oferty w okolicy<span class="blur w-100" style="left:-10%">Oferty w okolicy.</span><span style="font-size:48px; color: #dc3545;">.</span></h2>
                                            <?php while ($resultNear->have_posts()) : $resultNear->the_post(); ?>
                                                <div class="job-list half-grid">
                                                    <div class="thumb">
                                                        <a href="#">
                                                            <img style="max-width:70px !important;" src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-fluid" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="body">
                                                        <div class="content">
                                                            <h4>
                                                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                                            </h4>
                                                            <div class="info">
                                                                <span class="company"><a href="#"><i style="color: rgba(220, 53, 69, 0.3);" class="<?php echo get_field('ikona', get_field('kategoria', $post)[0]->ID); ?>"> </i> <?php echo get_field('kategoria')[0]->post_title; ?> </a> </span>
                                                                <span class="office-location"><a href="#"><i style="color: rgba(0, 204, 0, 0.3);" class="fas fa-map-marker-alt"></i> <?php echo get_field('map')['city']; ?>, <?php echo get_field('map')['state']; ?></a></span>
                                                                <span class="job-type temporary"><a href="#"><i style="color: rgba(77, 121, 255, 0.5);" class="far fa-clock"> </i> <?php echo  wpjm_get_the_job_types()[0]->name; ?> </a> </span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            <?php endwhile; ?>
                                        </div>
                                        <div class="col-lg-6 mt-3 mb-3 right">

                                            <h2 class="blur-par text-center">Nowe oferty<span class="blur w-100" style="left:-5%">Nowe oferty.</span><span style="font-size:48px; color: #dc3545;">.</span></h2>
                                            <a href="/job-list" class="text-center w-100" style="font-size:14px;">Zobacz wszystkie oferty pracy </a>
                                            <?php while ($result->have_posts()) : $result->the_post(); ?>
                                                <div class="job-list half-grid">
                                                    <div class="thumb">
                                                        <a href="<?php the_permalink(); ?>">
                                                            <img style="max-width:70px !important;" src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-fluid" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="body">
                                                        <div class="content">
                                                            <h4>
                                                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                                            </h4>
                                                            <div class="info">
                                                                <span class="company"><a href="#"><i style="    color: rgba(220, 53, 69, 0.3);" class="<?php echo get_field('ikona', get_field('kategoria', $post)[0]->ID); ?>"> </i> <?php echo get_field('kategoria')[0]->post_title; ?></a> </span>
                                                                <span class="office-location"><a href="#"><i style="color: rgba(0, 204, 0, 0.3);" class="fas fa-map-marker-alt"></i> <?php echo get_field('map')['city']; ?>, <?php echo get_field('map')['state']; ?></a></span>
                                                                <span class="job-type temporary"><a href="#"><i style="color: rgba(77, 121, 255, 0.5);" class="far fa-clock"> </i> <?php echo  wpjm_get_the_job_types()[0]->name; ?> </a> </span>
                                                            </div>
                                                        </div>
                                                        <div class="more">
                                                            <div class="buttons">
                                                                <a href="#" class="button">Więcej o ofercie</a>
                                                                <a href="#" class="favourite"><i data-feather="heart"></i></a>
                                                            </div>
                                                            <p class="deadline">Czas zakończenia: 21 Luty, 2018</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="first-dyngs" style="background: url(<?php echo DefaultHelper::imageDir('oferta-pracy-1.svg') ?>)">
                <div class="second-dyngs" style="background: url(<?php echo DefaultHelper::imageDir('oferta-pracy-2.svg') ?>)">
                    <div class="third-dyngs" style="background: url(<?php echo DefaultHelper::imageDir('oferta-pracy-3.svg') ?>)">

                    </div>
                </div>
            </div>
        </div>
        <!-- Jobs End -->

        <!-- Fun Facts -->
        <div class="padding-top-200 fact-bg">
            <div class="container">
                <div class="row fact-items">
                    <div class="col-md-3 col-sm-6">
                        <div class="fact">
                            <div class="fact-icon">
                                <img src="<?php echo DefaultHelper::imageDir('oferty-pracy.svg') ?>">
                            </div>
                            <p class="fact-number"><span class="count" data-form="0" data-to="12376">56</span></p>
                            <p class="fact-name">Ofert pracy</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="fact">
                            <div class="fact-icon">
                                <img src="<?php echo DefaultHelper::imageDir('kandydaci.svg') ?>">
                            </div>
                            <p class="fact-number"><span class="count" data-form="0" data-to="89562">987</span></p>
                            <p class="fact-name">Kandytatów</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="fact">
                            <div class="fact-icon">
                                <img src="<?php echo DefaultHelper::imageDir('cv-count.svg') ?>">
                            </div>
                            <p class="fact-number"><span class="count" data-form="0" data-to="28166">13</span></p>
                            <p class="fact-name">Złożonych CV</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="fact">
                            <div class="fact-icon">
                                <img src="<?php echo DefaultHelper::imageDir('companies.svg') ?>">
                            </div>
                            <p class="fact-number"><span class="count" data-form="0" data-to="1366">45</span></p>
                            <p class="fact-name">Firm</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fun Facts End -->

        <!-- Top Companies -->
        <div class="section-padding-top padding-bottom-185 ">
            <div class="container p-5">
                <div class="row">
                    <div class="col">
                        <div class="section-header">
                            <h2>Nasi partnerzy<span class="blur" style="left: 30%">Nasi partnerzy.</span><span style="font-size:48px; color: #dc3545;">.</span></h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="company-carousel owl-carousel">
                            <?php while ($result->have_posts()) : $result->the_post(); ?>
                                <div class="company-wrap">
                                    <div class="thumb">
                                        <a href="#">
                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-fluid" style="object-fit: contain; height: 100px;" alt="">
                                        </a>
                                    </div>
                                    <div class="body">
                                        <h4><a href="weblider.eu"><?php the_company_name('<strong>', '</strong>'); ?></a></h4>
                                        <span> <?php echo get_field('map')['city']; ?>, <?php echo get_field('map')['state']; ?></span>
                                        <a href="/jobs-list" class="button">Sprawdź oferty pracy</a>
                                    </div>
                                </div>
                            <?php endwhile;; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Top Companies End -->

        <!-- Flipowanie Box -->
        <div class="row">
            <div class="col">
                <div class="section-header">
                    <h2>Jesteś...<span class="blur" style="left:43%">Jesteś....</span><span style="font-size:48px; color: #dc3545;">.</span></h2>
                </div>
            </div>
        </div>
        <div class="d-flex flips">
            <div class="flip-card position-relative w-100">
                <div class="flip-card-inner">
                    <div class="flip-card-front">
                        <img src="<?php echo DefaultHelper::imageDir('pracodawca.png') ?>" />
                    </div>
                    <div class="flip-card-back" style="background-image: url(<?php echo DefaultHelper::imageDir('pracodawca_back.png') ?>);">
                        <h2>pracodawcą<span style="font-size:48px; color:#dc3545">?</span></h2>
                        <h5 class="flip-card-back-text">Sprawnie i z minimum formalności pomożemy Ci znaleźć odpowiednich ludzi do Twojego zespołu - zarówno w przypadku pracy tymczasowej, jak i stałej. Sprawdź, co dla Ciebie przygotowaliśmy.</h5>
                        <a href="#" role="button" class="btn btn-secondary btn-lg m-5" style="border:none; background:#dc3545; padding: 2rem; font-size: 2rem;">Poznaj szczegóły i możliwości współpracy</a>
                    </div>
                </div>
            </div>


            <div class="flip-card position-relative w-100">
                <div class="flip-card-inner">
                    <div class="flip-card-front">
                        <img src="<?php echo DefaultHelper::imageDir('kandydat.png') ?>" />
                    </div>
                    <div class="flip-card-back text-center" style="background-image: url(<?php echo DefaultHelper::imageDir('kandydat_back.png') ?>);">
                        <h2>pracownikiem<span style="font-size:48px; color:#dc3545">?</span></h2>
                        <h5 class="flip-card-back-text">Zyskaj legalną i dobrze płatną pracę w Polsce lub za granicą. Pomożemy Ci zorganizować wyjazd, w tym załatwić formalności związane z zatrudnieniem. Sprawdź, jak wygląda współpraca.</h5>
                        <a href="/jobs-list/" role="button" class="btn btn-secondary btn-lg m-5" style="border:none; background:#dc3545; padding: 2rem; font-size: 2rem;">Poznaj szczegóły i możliwości współpracy</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Registration Box End -->


        <!-- Testimonial -->
        <div class="section-padding-bottom section-padding-top" style="padding-top:0px!important;: 110%; background-position: right; background-repeat: no-repeat; background-image: url(<?php echo DefaultHelper::imageDir('kropki_new1.png') ?>)">
            <div class="container p-5">
                <div class="row">
                    <div class="col">
                        <div class="section-header">
                            <h2>Opinie<span class="blur" style="left:41%">Opinie.</span><span style="font-size:48px; color: #dc3545;">.</span></h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="testimonial">
                            <div class="testimonial-for">
                                <div class="testimonial-item d-flex flex-lg-column flex-column justify-content-center mx-auto my-0">
                                    <h2 class="pt-2 pb-2">Pracodawcy</h2>
                                    <div class="choose-your-profile">
                                        <img class="mx-auto d-block profile-pic" src="<?php echo DefaultHelper::imageDir('opinia-01.svg'); ?>">
                                    </div>
                                    <div class="d-flex align-items-center flex-column justify-content-center mx-auto my-0w pt-3 ">
                                        <p>“Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                                            deserunt mollit anim id est laborum.”</p>
                                        <h5>Tom</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="testimonial">
                            <div class="testimonial-for">
                                <div class="testimonial-item d-flex flex-lg-column flex-column justify-content-center mx-auto my-0">
                                    <h2 class="pt-2 pb-2">Pracownicy</h2>
                                    <div class="choose-your-profile">
                                        <img class="mx-auto d-block profile-pic " src="<?php echo DefaultHelper::imageDir('opinia-01.svg'); ?>">
                                    </div>
                                    <div class="d-flex align-items-center flex-column justify-content-center mx-auto my-0w pt-3">
                                        <p>“Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                                            deserunt mollit anim id est laborum.”</p>
                                        <h5>Tom</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Testimonial End -->




        <!-- News -->
        <div class="news-container padding-top-90  mb-5">
            <div class="container-fluid mb-5">
                <div class="row">
                    <div class="col">
                        <div class="section-header">
                            <h2>Co nowego na blogu<span class="blur" style="left:30%">Co nowego na blogu?</span><span style="font-size:48px; color: #dc3545;">?</span></h2>
                            <p>Piszemy o pracy, rekrutacji, nowych ofertach i branżowych nowinkach
                                - bądź na bieżąco!
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="main-carousel w-100" data-flickity='{ "cellAlign": "left", "contain": true, "wrapAround":false, "pageDots":false }'>
                        <?php $count = 1; foreach($news->posts as $new){ ?>
                            <a href="<?php echo get_the_permalink($new); ?>" class="col-lg-4 col-md-4 col-sm-6 pl-2 pr-2" id="<?php echo $count; ?>" >
                                <div class="card-news-whole">
                                    <div class="image-contain">
                                        <img class="card-img-top card-news "
                                             src="<?php echo get_the_post_thumbnail_url($new); ?>" />
                                    </div>
                                    <div class="card-body text-center pt-3 mt-auto" >
                                        <h2><?php echo get_the_title($new); ?></h2>
                                    </div>
                                </div>
                            </a>
                            <?php $count++;} ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- NewsLetter End -->

        <!-- NewsLetter -->
        <div class="newsletter-bg padding-top-90 mt-5 mb-5 ">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6">
                        <div class="newsletter-wrap">
                            <h2>Masz pytania?</h2>
                            <h6>+48 123 123 123</h6>
                            <h3>Skontaktuj się z nami!</h3>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- NewsLetter End -->
    </div>

<?php get_footer();
