<?php
/*
 * Template Name: Kontakt
 */

get_header();
?>
<!-- Breadcrumb -->
<div class="alice-bg padding-top-150 padding-bottom-70">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="breadcrumb-area">
                    <h1><?php the_title() ?></h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Strona główna</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?php the_title() ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-6">
                <div class="breadcrumb-form">
                    <form action="#">
                        <input type="text" placeholder="Enter Keywords">
                        <button><i data-feather="search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->
<!-- Breadcrumb End -->

<div class="alice-bg padding-top-150 section-padding-bottom">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="contact-block">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="contact-address">
                                <h4>Skontaktuj się z nami</h4>
                                <ul>
                                    <li><i data-feather="map-pin"></i>Weblider.eu</li>
                                    <li><i data-feather="mail"></i>kontakt@weblider.eu</li>
                                    <li><i data-feather="phone-call"></i>+123 123 123</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-7 offset-lg-1">
                            <div class="contact-form">
                                <h4>Napisz do nas</h4>
                                <form action="#" id="contactForm">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Imię i nazwisko">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="email" class="form-control" placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Telefon">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Temat">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <textarea class="form-control" placeholder="Treść wiadomości"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="button">Wyślij</button>
                                    <p class="input-success">Your message has been sent. Thanks for contact.</p>
                                    <p class="input-error">Something went wrong while sending. Please try leter.</p>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div id="map-area" class="contact-map">
                                <div class="cp-map" id="location" data-lat="40.713355" data-lng="-74.005535" data-zoom="10"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

