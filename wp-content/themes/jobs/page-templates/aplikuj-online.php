<?php
/*
 * Template Name: Podstawowa templatka
 */

get_header();
?>


<!-- Breadcrumb -->
    <div class="alice-bg padding-top-150">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6" style="align-self: center">
                    <div class="breadcrumb-area" style="text-align: center">
                        <h1 style=""><?php the_title() ?></h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <img class="img-fluid" style="max-height: 250px; width:100%; object-fit: cover" src="<?php echo DefaultHelper::imageDir('worker.jpg') ?>" />
                </div>
            </div>
        </div>
    </div>
<!-- Breadcrumb End -->

    <!-- Candidates Details -->
    <div class="alice-bg padding-top-60 section-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="blog-post-details-container">
                        <div class="blog-details-wrapper">
                            <div class="post-content">
                                <p><?php the_content(); ?></p>
                            </div>
                        </div>
                        <div class="post-meta">
                            <div class="post-author">
                                <div class="avatar">
                                    <img src="images/blog/author.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="name">
                                    <p>Dodane przez: </p>
                                    <h5><a href="#"><?php the_author(); ?></a></h5>
                                </div>
                            </div>
                            <div class="post-tag">
                                <h6>Tagi wpisu</h6>
                                <div class="tags">
                                    <a href="#">Design</a>
                                    <a href="#">Creative</a>
                                    <a href="#">Photoshop</a>
                                    <a href="#">Tech</a>
                                </div>
                            </div>
                            <div class="post-share">
                                <h6>Udostępnij</h6>
                                <div class="social-buttons">
                                    <a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
                                    <a href="#" class="google"><i class="fab fa-google-plus-g"></i></a>
                                    <a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a>
                                    <a href="#" class="pinterest"><i class="fab fa-pinterest-p"></i></a>
                                    <a href="#" class="instagram"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer();
