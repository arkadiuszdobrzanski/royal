<?php
/*
 * Template Name: Pojedyncza aktualnosc
 * Template Post Type: post, page, product
*/

get_header();


?>

<?php while ( have_posts() ) : the_post(); ?>
    <div class="alice-bg padding-top-150">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6" style="align-self: center">
                    <div class="breadcrumb-area" style="text-align: center">
                        <h1 style=""><?php the_title() ?></h1>
                    </div>
                </div>
                <div class="col-md-6">
                <img class="img-fluid" style="max-height: 250px; width:100%; object-fit: cover" src="<?php echo DefaultHelper::imageDir('worker.jpg') ?>" />
                </div>
            </div>
        </div>
    </div>
    <div class="alice-bg section-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="blog-post-details-container">
                        <div class="blog-details-wrapper">
                            <div class="info">
                                <span class="date"><?php echo the_date('d F Y'); ?></span>
                                <span class="author"><a href="#"><i data-feather="user"></i>Autor: <?php the_author(); ?></a></span>
<!--                                <span class="comments"><i data-feather="message-circle"></i>42</span>-->
                            </div>
                            <div class="post-content">
                                <img src="<?php echo the_post_thumbnail_url(); ?>" class="img-fluid w-100" />

                                <p><?php the_content(); ?></p>
                            </div>
                        </div>
                        <div class="post-meta">
                            <div class="post-author">
                                <div class="avatar">
                                    <img src="images/blog/author.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="name">
                                    <p>Dodane przez: </p>
                                    <h5><a href="#"><?php the_author(); ?></a></h5>
                                </div>
                            </div>
                            <div class="post-tag">
                                <h6>Tagi wpisu</h6>
                                <div class="tags">
                                    <a href="#">Design</a>
                                    <a href="#">Creative</a>
                                    <a href="#">Photoshop</a>
                                    <a href="#">Tech</a>
                                </div>
                            </div>
                            <div class="post-share">
                                <h6>Udostępnij</h6>
                                <div class="social-buttons">
                                    <a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
                                    <a href="#" class="google"><i class="fab fa-google-plus-g"></i></a>
                                    <a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a>
                                    <a href="#" class="pinterest"><i class="fab fa-pinterest-p"></i></a>
                                    <a href="#" class="instagram"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="post-comment-block">
                            <div class="comment-respond">
                                <h4>Napisz komentarz</h4>
                                <form action="#">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" placeholder="Imię" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="email" placeholder="Email" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea placeholder="Treść komentarza" class="form-control"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <input class="custom-radio" type="checkbox" id="radio-4" name="termsandcondition">
                                                <label for="radio-4">
                                                    <span class="dot"></span> Powiadom mnie mailowo o odpowiedzi na komentarz.
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="button primary-bg">Dodaj</button>
                                </form>
                            </div>
                            <div class="comment-area">
                                <h4>5 komentarzy</h4>
                                <ul class="comments">
                                    <li class="comment">
                                        <div class="comment-wrap">
                                            <div class="comment-info">
                                                <div class="commenter-thumb">
                                                    <img src="images/blog/2.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="commenter-info">
                                                    <span class="commenter-name">Lisa Parsons</span>
                                                    <span class="date">28 Dec 2020</span>
                                                </div>
                                                <div class="reply">
                                                    <button type="button" class="reply-button" data-toggle="modal" data-target="#modal-skill25">Reply</button>
                                                    <div class="modal fade" id="modal-skill25" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    <div class="comment-respond">
                                                                        <h4>Write A Comment</h4>
                                                                        <form action="#">
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <input type="text" placeholder="Name" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <input type="email" placeholder="Email address" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <textarea placeholder="Comment" class="form-control"></textarea>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <input class="custom-radio" type="checkbox" id="radio-6" name="termsandcondition2">
                                                                                        <label for="radio-6">
                                                                                            <span class="dot"></span> Notify me of follow-up comments by email.
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <button class="button primary-bg">Submit</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="comment-body">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                            </div>
                                        </div>
                                        <ul class="comments child-comment">
                                            <li class="comment">
                                                <div class="comment-wrap">
                                                    <div class="comment-info">
                                                        <div class="commenter-thumb">
                                                            <img src="images/blog/3.jpg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="commenter-info">
                                                            <span class="commenter-name">Ethel N. Myers</span>
                                                            <span class="date">28 Dec 2020</span>
                                                        </div>
                                                        <div class="reply">
                                                            <button type="button" class="reply-button" data-toggle="modal" data-target="#modal-skill26">Reply</button>
                                                            <div class="modal fade" id="modal-skill26" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-body">
                                                                            <div class="comment-respond">
                                                                                <h4>Write A Comment</h4>
                                                                                <form action="#">
                                                                                    <div class="row">
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <input type="text" placeholder="Name" class="form-control">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <input type="email" placeholder="Email address" class="form-control">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div class="form-group">
                                                                                                <textarea placeholder="Comment" class="form-control"></textarea>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <input class="custom-radio" type="checkbox" id="radio-7" name="termsandcondition3">
                                                                                                <label for="radio-7">
                                                                                                    <span class="dot"></span> Notify me of follow-up comments by email.
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <button class="button primary-bg">Submit</button>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="comment-body">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="comment">
                                        <div class="comment-wrap">
                                            <div class="comment-info">
                                                <div class="commenter-thumb">
                                                    <img src="images/blog/1.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="commenter-info">
                                                    <span class="commenter-name">Rovert Carlos</span>
                                                    <span class="date">28 Dec 2020</span>
                                                </div>
                                                <div class="reply">
                                                    <button type="button" class="reply-button" data-toggle="modal" data-target="#modal-skill27">Reply</button>
                                                    <div class="modal fade" id="modal-skill27" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    <div class="comment-respond">
                                                                        <h4>Write A Comment</h4>
                                                                        <form action="#">
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <input type="text" placeholder="Name" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <input type="email" placeholder="Email address" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <textarea placeholder="Comment" class="form-control"></textarea>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <input class="custom-radio" type="checkbox" id="radio-8" name="termsandcondition8">
                                                                                        <label for="radio-8">
                                                                                            <span class="dot"></span> Notify me of follow-up comments by email.
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <button class="button primary-bg">Submit</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="comment-body">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="view-all">
                                    <a href="#">View all comments</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Candidates Details End -->

    <div class="apply-popup">
        <div class="modal fade" id="apply-popup-id" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><i data-feather="edit"></i>Aplikuj teraz</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="#">
                            <div class="form-group">
                                <input type="text" placeholder="Imię i nazwisko" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="email" placeholder="Adres email" class="form-control">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Wiadomość"></textarea>
                            </div>
                            <div class="form-group file-input-wrap">
                                <label for="up-cv">
                                    <input id="up-cv" type="file">
                                    <i data-feather="upload-cloud"></i>
                                    <span>Załącz swoje CV <span>(pdf,zip,doc,docx)</span></span>
                                </label>
                            </div>
                            <div class="more-option">
                                <input class="custom-radio" type="checkbox" id="radio-4" name="termsandcondition">
                                <label for="radio-4">
                                    <span class="dot"></span> Akceptuje <a href="#">regulamin</a>
                                </label>
                            </div>
                            <button class="button primary-bg btn-block">Aplikuj teraz</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- NewsLetter -->
    <div class="newsletter-bg padding-top-90 padding-bottom-90">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-6">
                    <div class="newsletter-wrap">
                        <h3>Newsletter</h3>
                        <p>Wpisz swój adres e-mail jeśli chcesz dostawać oferty pracy</p>
                        <form action="#" class="newsletter-form form-inline">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Email address...">
                            </div>
                            <button class="btn button">Wyślij<i class="fas fa-caret-right"></i></button>
                            <p class="newsletter-error">0 - Please enter a value</p>
                            <p class="newsletter-success">Thank you for subscribing!</p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- NewsLetter End -->

<?php endwhile; ?>

<?php get_footer();
