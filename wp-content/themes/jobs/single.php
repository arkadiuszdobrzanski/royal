<?php
/*
 * Template Name: Oferta pracy
 * Template Post Type: post, page, product
*/

get_header();

$types = wpjm_get_the_job_types();
//var_dump($post);
$result = ProductsService::getJobs();
?>

<?php while ( have_posts() ) : the_post(); ?>

    <!-- Candidates Details -->
    <div class="alice-bg padding-top-150 section-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="job-listing-details">
                        <div class="job-title-and-info">
                            <div class="title">
                                <div class="thumb">
                                    <img src="<?php echo get_the_post_thumbnail_url();?>" class="img-fluid pr-2 pl-2 " alt="">
                                </div>
                                <div class="title-body">
                                    <h4><?php echo the_title(); ?></h4>
<!--                                --><?php //var_dump($types); ?>
                                    <div class="info">
                                        <span class="company"><a href="#"><i data-feather="briefcase"></i><?php the_company_name( '<strong>', '</strong>' ); ?></a></span>
                                        <span class="office-location"><a href="#"><i data-feather="map-pin"></i> <?php echo get_field('map')['city']; ?>, <?php echo get_field('map')['state']; ?></a></span>
                                        <span class="job-type full-time"><a href="#"><i data-feather="clock"></i> <?php echo $types[0]->name; ?> </a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="buttons">
<!--                                <a class="save" href="#"><i data-feather="heart"></i>Zapisz ofertę</a>-->
                                <a class="apply" href="#" data-toggle="modal" data-target="#apply-popup-id">Aplikuj Online</a>
                            </div>
                        </div>
                        <div class="details-information section-padding-60">
                            <div class="row">
                                <div class="col-xl-7 col-lg-8">
                                    <div class="description details-section">
                                        <h4><i data-feather="align-left"></i>Opis oferty</h4>
                                        <?php wpjm_the_job_description(); ?>

                                         </div>

                                    <?php if(get_field('zajecie')) { ?>
                                    <div class="responsibilities details-section">
                                        <h4><i data-feather="zap"></i>Czym będziesz się zajmował?</h4>
                                        <?php echo get_field('zajecie'); ?>
                                    </div>
                                    <?php } ?>

                                    <?php if(get_field('wymagane_doswiadczenie')) { ?>
                                    <div class="edication-and-experience details-section">
                                        <h4><i data-feather="book"></i>Wymagane doświadczenie</h4>
                                        <?php echo get_field('wymagane_doswiadczenie'); ?>
                                    </div>
                                    <?php } ?>
                                    <?php if(get_field('benefity')) { ?>
                                    <div class="other-benifit details-section">
                                        <h4><i data-feather="gift"></i>Benefity</h4>
                                        <?php echo get_field('benefity'); ?>
                                    </div>
                                    <?php } ?>
                                    <div class="job-apply-buttons">
                                        <a href="#" class="apply"  data-toggle="modal" data-target="#apply-popup-id">Aplikuj online</a>
                                        <a href="#" class="email"><i data-feather="mail"></i>Skontaktuj się z pracodawcą</a>
                                    </div>
                                </div>
                                <div class="col-xl-4 offset-xl-1 col-lg-4">
                                    <div class="information-and-share">
                                        <div class="job-summary">
                                            <h4>Podsumowanie</h4>
                                            <ul>
                                                <li><span>Data publikacji:</span><?php the_job_publish_date(); ?></li>
                                                <li><span>Etat:</span> <?php echo $types[0]->name; ?></li>
                                                <li><span>Wymagane doświadczenie:</span> <?php echo get_field('doswiadczenie'); ?> </li>
                                                <li><span>Lokalizacja:</span> <?php echo get_field('map')['city']; ?>, <?php echo get_field('map')['state']; ?> </li>
                                                <li><span>Wynagrodzenie:</span> <?php echo get_field('wynagrodzenie'); ?></li>
                                                <li><span>Czas trwania oferty:</span> <?php ?></li>
                                            </ul>
                                        </div>
                                        <div class="share-job-post">
                                            <span class="share"><i class="fas fa-share-alt"></i>Udostępnij:</span>
                                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                                            <a href="#"><i class="fab fa-twitter"></i></a>
                                            <a href="#"><i class="fab fa-linkedin-in"></i></a>
                                            <a href="#"><i class="fab fa-google-plus-g"></i></a>
                                        </div>
                                        <div class="buttons">
                                            <a href="#" class="button print"><i data-feather="printer"></i>Wydrukuj</a>
                                            <a href="#" class="button report"><i data-feather="flag"></i>Zgłoś</a>
                                        </div>
                                        <div class="job-location">
                                            <h4>Lokalizacja na mapie</h4>
                                            <div id="map-area">
                                                <div class="cp-map" id="location" data-lat="<?php echo get_field('map')['lat'];  ?>" data-lng="<?php echo get_field('map')['lng'];  ?>" data-zoom="10"></div>
                                                <!-- <div class="cp-map" id="location" data-lat="40.713355" data-lng="-74.005535" data-zoom="10"></div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-7 col-lg-8">
                                <div class="company-information details-section">
                                    <h4><i data-feather="briefcase"></i>O firmie</h4>
                                    <ul>
                                        <li><span>Nazwa firmy:</span> <?php the_company_name( '<strong>', '</strong>' ); ?></li>
                                        <li><span>Adres:</span> Labędzka 22</li>
                                        <li><span>Strona :</span> <a href="<?php echo esc_url( get_the_company_website() ); ?>"><?php echo esc_url( get_the_company_website() ); ?></a></li>
                                        <li><span>Informacje:</span></li>
                                        <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum looked up one of the more obscure Latin words, consectetur.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Candidates Details End -->

    <div class="apply-popup">
        <div class="modal fade" id="apply-popup-id" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><i data-feather="edit"></i>Aplikuj teraz</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="#">
                            <div class="form-group">
                                <input type="text" placeholder="Imię i nazwisko" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="email" placeholder="Adres email" class="form-control">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Wiadomość"></textarea>
                            </div>
                            <div class="form-group file-input-wrap">
                                <label for="up-cv">
                                    <input id="up-cv" type="file">
                                    <i data-feather="upload-cloud"></i>
                                    <span>Załącz swoje CV <span>(pdf,zip,doc,docx)</span></span>
                                </label>
                            </div>
                            <div class="more-option">
                                <input class="custom-radio" type="checkbox" id="radio-4" name="termsandcondition">
                                <label for="radio-4">
                                    <span class="dot"></span> Akceptuje <a href="#">regulamin</a>
                                </label>
                            </div>
                            <button class="button primary-bg btn-block">Aplikuj teraz</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Jobs -->
    <div class="section-padding-bottom alice-bg">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section-header section-header-2 section-header-with-right-content">
                        <h2>Podobne oferty</h2>
                        <a href="/jobs-list" class="header-right">+ Wróć do listy ofert</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
    <?php while ($result->have_posts()) : $result->the_post(); ?>
                    <div class="job-list">
                        <div class="thumb">
                            <a href="<?php the_permalink(); ?>">
                                <img style="max-width:70px !important;"
                                     src="<?php echo get_the_post_thumbnail_url(); ?>"
                                     class="img-fluid" alt="">
                            </a>
                        </div>
                        <div class="body">
                            <div class="content">
                                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                <div class="info">
                                    <span class="company"><a href="#"><i style="    color: rgba(220, 53, 69, 0.3);" class="<?php echo get_field('ikona',get_field('kategoria', $post)[0]->ID); ?>"> </i> <?php echo get_field('kategoria')[0]->post_title; ?></a></span>
                                    <span class="office-location"><a href="#"><i  style="color: rgba(0, 204, 0, 0.3);" class="fas fa-map-marker-alt"> </i>  <?php echo get_field('map')['city']; ?>, <?php echo get_field('map')['state']; ?></a></span>
                                    <span class="job-type full-time"><a href="#"><i style="color: rgba(77, 121, 255, 0.3);" class="far fa-clock"> </i> <?php echo wpjm_get_the_job_types()[0]->name; ?></a></span>
                                </div>
                            </div>
                            <div class="more">
                                <div class="buttons">
                                    <a href="<?php the_permalink(); ?>" class="button">Więcej informacji</a>
                                    <a class="apply" href="#" data-toggle="modal" data-target="#apply-popup-id">Aplikuj</i></a>
                                </div>
                                <p class="deadline">Oferta wygasa: 31 Grudzień,  2020</p>
                            </div>
                        </div>
                    </div>
    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Jobs End -->

    <!-- NewsLetter -->
    <div class="newsletter-bg padding-top-90 padding-bottom-90">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-6">
                    <div class="newsletter-wrap">
                        <h3>Newsletter</h3>
                        <p>Wpisz swój adres e-mail jeśli chcesz dostawać oferty pracy</p>
                        <form action="#" class="newsletter-form form-inline">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Email address...">
                            </div>
                            <button class="btn button">Wyślij<i class="fas fa-caret-right"></i></button>
                            <p class="newsletter-error">0 - Please enter a value</p>
                            <p class="newsletter-success">Thank you for subscribing!</p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- NewsLetter End -->

<?php endwhile; ?>

<?php get_footer();
