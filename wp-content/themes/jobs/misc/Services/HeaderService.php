<?php



class HeaderService
{
    private $headerHeight;

    private $headerImg;

    private $vitamedText;

    private $titleText;

    private $maskHeight;

    private $maskImg;

    private $logo;

    private $class;

    private $button;

    private $productImg;

    private $iconLekarz;

    private $iconFarmaceuta;

    public $wpQuery;

    public function __construct($wpQuery)
    {
        $this->wpQuery = $wpQuery;
        $this->getContent();

    }
    public function getTitleText()
    {
        return $this->titleText;
    }

    public function getButton()
    {
        return $this->button;
    }

    public function getClass()
    {
        return $this->class;
    }

    public function getProductImg()
    {
        return $this->productImg;
    }

    public function getVitamedText()
    {
        return $this->vitamedText;
    }

    public function getHeaderHeight()
    {
        return $this->headerHeight;
    }

    public function getHeaderImg()
    {
        return $this->headerImg;
    }

    public function getLogo(){
        return $this->logo;
    }

    public function getMaskHeight()
    {
        return $this->maskHeight;
    }

    public function getMaskImg()
    {
        return $this->maskImg;
    }

    public function getIconLekarz()
    {
        return $this->iconLekarz;
    }

    public function getIconFarmaceuta()
    {
        return $this->iconFarmaceuta;
    }

    public function setHeaderHeight($headerHeight)
    {
        $this->headerHeight = $headerHeight;

    }

    public function setTitleText($titleText)
    {
        $this->titleText = $titleText;
    }

    public function setVitamedText($vitamedText)
    {
       $this->vitamedText = $vitamedText;
    }

    public function setHeaderImg($headerImg)
    {
        $this->headerImg = $headerImg;

    }

    public function setLogo($logo){
        $this->logo = $logo;

    }

    public function setButton($button){
        $this->button = $button;

    }

    public function setMaskHeight($maskHeight)
    {
        $this->maskHeight = $maskHeight;

    }

    public function setMaskImg($maskImg)
    {
        $this->maskImg = $maskImg;

    }

    public function setIconLekarz($iconLekarz)
    {
        $this->iconLekarz = $iconLekarz;

    }

    public function setIconFarmaceuta($iconFarmaceuta)
    {
        $this->iconFarmaceuta = $iconFarmaceuta;

    }

    public function setProductImg($productImg){
        $this->productImg = $productImg;
    }


    public function isHome(){
        if(!is_front_page()){
            return false;
        }else{
            return true;
        }
    }


    public function getContent(){
        //jeśli NIE JEST stroną główną
        if(!is_front_page()){
            if($this->isProduct()){
                $this->setProductImg(get_the_post_thumbnail_url('full'));
            }
            $this->setLogo($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/logo@2.png');
            $this->setIconLekarz($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/ikona-dla-lekarzy-granatowa@2.png');
            $this->setIconFarmaceuta($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/ikona-dla-farmaceutow-granatowa@2.png');
            $this->setHeaderHeight('50vh');
            $this->setMaskHeight('50vh');
            if($this->isProduct()){
                $this->setHeaderImg(null);
                $this->setMaskImg($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/maskowanie-podstrona.png');
            }else{
                $this->setHeaderImg($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/'.$this->checkPage());
                $this->setMaskImg($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/maskowanie-podstrona.png');
            }
            $this->setTitleText('<h2 class="header-title">'.mb_strtoupper($this->wpQuery->post->post_title.'</h2>'));

        }else{
            $this->setLogo($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/logo-biale@2.png');
            $this->setIconLekarz($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/ikona-dla-lekarzy-biala@2.png');
            $this->setIconFarmaceuta($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/ikona-dla-farmaceutow-biala@2.png');
            $this->setHeaderHeight('70vh');
            $this->setMaskHeight('70vh');
            $this->setHeaderImg($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/'.$this->checkPage());
            $this->setMaskImg($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/maskowanie-podstrona.png');
            $this->setTitleText('<h2 class="header-title-bold">'.mb_strtoupper($this->wpQuery->post->post_title).'</h2>');
            $this->setButton('<button class="header-button">Poznaj nasze preparaty</button>');
        }
    }

    public function getPageName(){
        $post = $this->wpQuery->get_queried_object();
        return $post->post_name;
    }

    public function getPageTitle(){
        $post = $this->wpQuery->get_queried_object();
        return $post->post_title;
    }


    public function checkPage() : string
    {
        $pagename = $this->getPageName();

        if ($pagename == "kariera") {
            return 'tlo-kariera.jpg';
        }
        else if ($pagename == "aktualnosci") {
            return 'tlo-aktualnosci.jpg';
        }
        else if ($pagename == "pediatria" || $pagename == "dermatologia" || $pagename == "laryngologia" || $pagename == "urologia" || $pagename == "medycyna-rodzinna" || $pagename == "gastroenetrologia") {
            return 'tlo-pediatria.jpg';
        }
        else if ($pagename == "wspolpraca") {
            return 'tlo-1.jpg';
        }
        else if ($pagename == "home" || $pagename == "lista-produktow") {
            return 'slajd-lato.jpg';
        }else{
            return 'przykladowy-wpis-aktualnosci2.jpg';
        }
    }

    public function isProduct()
    {
        if(get_post_meta(get_the_ID(), '_wp_page_template', TRUE) == 'single-product.php'){
            return true;
        }else{
            return false;
        }
    }

}