<?php


class LinksHelper
{
    function __construct()
    {
        add_action('admin_enqueue_scripts', 'site_script');
    }

    function site_script() {
        wp_enqueue_script('custom-script', get_template_directory_uri().'assets/js/script.js', array(), '1.0', true);
    }
}