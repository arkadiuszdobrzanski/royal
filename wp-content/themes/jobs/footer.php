<?php

?>
<!-- Footer -->
<footer class="footer-bg">
    <div class="footer-top pb-2" >
        <div class="container">
            <div class="row">

            </div>
        </div>
    </div>
    <div class="footer-widget-wrapper pt-5 pb-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <div class="footer-widget widget-about">
                        <h4>O nas</h4>
                        <div class="widget-inner">
                            <p class="description">Nazywamy się Royal Teamworker i jesteśmy agencją pośrednictwa pracy.
                                Rekrutujemy ludzi dla ludzi.</p>
                            <span class="about-contact"><i data-feather="phone-forwarded"></i>+48 123 123 123</span>
                            <span class="about-contact"><i data-feather="mail"></i>weblider@weblider.eu</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 offset-lg-1 col-sm-6">
                    <div class="footer-widget footer-shortcut-link">
                        <h4>Informacje</h4>
                        <div class="widget-inner">
                            <ul>
                                <li><a href="#">O nas</a></li>
                                <li><a href="#">Skontaktuj się z nami</a></li>
                                <li><a href="#">Regulamin</a></li>
                                <!--                                <li><a href="#">Terms &amp; Conditions</a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="footer-widget footer-shortcut-link">
                        <h4>Kandydat</h4>
                        <div class="widget-inner">
                            <ul>
                                <!--                                <li><a href="#">Create Account</a></li>-->
                                <!--                                <li><a href="#">Career Counseling</a></li>-->
                                <!--                                <li><a href="#">My Oficiona</a></li>-->
                                <!--                                <li><a href="#">FAQ</a></li>-->
                                <li><a href="#">Regulamin</a></li>
                                <!--                                <li><a href="#">Video Guides</a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="footer-widget footer-shortcut-link">
                        <h4>Pracodawca</h4>
                        <div class="widget-inner">
                            <ul>
                                <!--                                <li><a href="#">Create Account</a></li>-->
                                <!--                                <li><a href="#">Products/Service</a></li>-->
                                <!--                                <li><a href="#">Post a Job</a></li>-->
                                <li><a href="#">Regulamin</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="">
                        <div class="row">
                            <div class="col-xl-4 col-lg-3 pb-4">
                                <div class="footer-logo">
                                    <a href="#">
                                        <img src="https://inmode.pl/wp-content/themes/weblider/img/weblider.png" class="img-fluid" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-3 pb-5">
                                <div class="footer-social pb-2">
                                    <ul class="social-icons mx-0 my-0">
                                        <li><a href="#"><i data-feather="facebook"></i></a></li>
                                        <li><a href="#"><i data-feather="twitter"></i></a></li>
                                        <li><a href="#"><i data-feather="instagram"></i></a></li>
                                        <li><a href="#"><i data-feather="youtube"></i></a></li>
                                    </ul>


                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-3 order-lg-3 text-center">
                                <div class="back-to-top">
                                    <a href="#">Powrót na górę  <i class="fas fa-angle-up"></i></a>
                                    <select class="selectpicker" class="pl-4" onchange="location = this.value;" data-width="fit">
                                        <option value="<?php echo site_url().'/'; ?> " data-content='<a href="/"><span class="flag-icon flag-icon-pl"></span> Polski</a>'>Polski</option>
                                        <option value="<?php echo site_url().'/en'; ?>" data-content='<a href="/en"><span class="flag-icon flag-icon-us"></span> English</a>'>English</option>
                                        <option value="<?php echo site_url().'/ru'; ?>" data-content='<a href="/ru"><span class="flag-icon flag-icon-ru"></span> Russian</a>'>Russian</option>
                                    </select>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->



<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/popper.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/feather.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap-select.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery.nstSlider.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/visible.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery.countTo.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/chart.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/plyr.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/tinymce.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery.ajaxchimp.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/ajax.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/nav.js"></script>
<script src="https://unpkg.com/scrollreveal"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/animations.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/custom.js"></script>
<script defer src="<?php bloginfo('template_directory'); ?>/node_modules/@fortawesome/fontawesome-free/js/brands.js"></script>
<script defer src="<?php bloginfo('template_directory'); ?>/node_modules/@fortawesome/fontawesome-free/js/solid.js"></script>
<script defer src="<?php bloginfo('template_directory'); ?>/node_modules/@fortawesome/fontawesome-free/js/fontawesome.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB832_bHfRQLmwHuIxiTy8K-5_REW1AAlY&libraries=places&callback=initAutocomplete" async defer></script>
<script src="<?php bloginfo('template_directory'); ?>/js/googleAPI.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/map.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/slick.min.js"></script>
<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>

<script>
    $(function(){
        $('.selectpicker').selectpicker();
    });


    $( document ).ready(function() {
        let karuzela = $('.main-carousel').find('.flickity-slider').children();
        console.log(karuzela);
        karuzela.first().css("border-top-right-radius", "25px");
    });

    Flickity.createMethods.push('_createPrevNextCells');

    Flickity.prototype._createPrevNextCells = function() {
        this.on( 'select', this.setPrevNextCells );
    };

    Flickity.prototype.setPrevNextCells = function() {
        // remove classes
        changeSlideClasses( this.previousSlide, 'remove', 'is-previous' );
        changeSlideClasses( this.nextSlide, 'remove', 'is-next' );
        // set slides
        this.previousSlide = this.slides[ this.selectedIndex - 1 ];
        this.nextSlide = this.slides[ this.selectedIndex + 1 ];
        this.nextnextSlide = this.slides[ this.selectedIndex + 2 ];
        // add classes
        changeSlideClasses( this.previousSlide, 'add', 'is-previous' );
        changeSlideClasses( this.nextSlide, 'add', 'is-next' );
        changeSlideClasses( this.nextnextSlide, 'add', 'is-nextnext' );
    };

    function changeSlideClasses( slide, method, className ) {
        if ( !slide ) {
            return;
        }
        slide.getCellElements().forEach( function( cellElem ) {
            cellElem.classList[ method ]( className );
        });
    }


</script>
</body>
</html>
